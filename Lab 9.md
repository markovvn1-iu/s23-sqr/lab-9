# Lab 9

Testing manually [goodfon.ru](https://www.goodfon.ru/) using [OWASP CSS (OWASP Cheat Sheet Series)](https://cheatsheetseries.owasp.org/index.html)

SQL Injection: [link](https://owasp.org/www-project-top-ten/2017/A1_2017-Injection.html)

Cross-Site Scripting (XSS): [link](https://owasp.org/www-project-top-ten/2017/A7_2017-Cross-Site_Scripting_(XSS).html)

## Testing 1 - Forgot password

**OWASP Spreadsheet**: [Forgot Password](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html)

| Test step                                         | Expected result | Result                                                       |
| ------------------------------------------------- | --------------- | ------------------------------------------------------------ |
| Open [goodfon.ru](https://www.goodfon.ru/)        | OK              | OK                                                           |
| Click the Sign In button in the header            | OK              | OK, navigated to [Sign in page](https://www.goodfon.ru/auth/) |
| Click on "Forgot Password?"                       | OK              | OK, navigated to reset password page                         |
| Entered a random non-existing email               | Error message   | Неправильный формат email-a                                  |
| Enter `'; DROP TABLE users; --` in the search box | Error message   | Неправильный формат email-a                                  |

## Testing 2 - Search form

| Test step                                                    | Expected result     | Result                                                  |
| ------------------------------------------------------------ | ------------------- | ------------------------------------------------------- |
| Open [goodfon.ru](https://www.goodfon.ru/)                   | OK                  | OK                                                      |
| Click on the search bar                                      | OK                  | OK                                                      |
| Enter `'; DROP TABLE users; --` in the search box            | No results found    | По запросу `'; DROP TABLE users; --` ничего не найдено. |
| Enter `><script>alert("XSS Attack!")</script>` in the search box | Script not executed | Script not executed                                     |

## Testing 3 - Login form

| Test step                                                    | Expected result     | Result                                           |
| ------------------------------------------------------------ | ------------------- | ------------------------------------------------ |
| Open [goodfon.ru](https://www.goodfon.ru/)                   | OK                  | OK                                               |
| Click on the search bar                                      | OK                  | OK                                               |
| Enter `'; DROP TABLE users; --` in the login form            | Error message       | Не угадали логин или пароль. Попробуйте еще раз. |
| Enter `><script>alert("XSS Attack!")</script>` in the login form | Script not executed | Script not executed                              |

## Testing 4 - URL Parameters

| Test step                                                    | Expected result         | Result                                                  |
| ------------------------------------------------------------ | ----------------------- | ------------------------------------------------------- |
| Open [goodfon.ru](https://www.goodfon.ru/)                   | OK                      | OK                                                      |
| Click on the search bar                                      | OK                      | OK                                                      |
| Enter "cat" in the login form                                | URL changed to `?q=cat` | URL changed to `?q=cat`                                 |
| Enter `'; DROP TABLE users; --` instead of cat in the URL    | No results found        | По запросу `'; DROP TABLE users; --` ничего не найдено. |
| Enter `><script>alert("XSS Attack!")</script>` instead of cat  in the URL | Script not executed     | Script not executed                                     |

